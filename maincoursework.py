class StockItem: 
#class variable
    StockCategory ="Car accessories"
    
    def __init__(self, quantity, price, stockcode, stockname,stockdescription, standardpercentageVAT= 1.2):    
#In the constructor, I made the attributes both private and public. Private because the instruction asked us to do so,
#and public because I wanted the NavSys subclass to have access to it
#also, I set the VAT 
        self.__quantity = quantity
        self.__price = price
        self.__stockcode = stockcode
        self.stockcode = stockcode
        self.quantity = quantity
        self.stockname = stockname
        self.stockdescription = stockdescription
        self.standardpercentageVAT = standardpercentageVAT
        self.price = price
 
#getter for private quantity       
    def getQuantity(self):
        return self.__quantity   
    
#getter for private price      
    def getPrice(self):
        return self.__price

#getter for private price with VAT (I used a 20 percent VAT, which means to multiplying the price without VAT by 1.2)   
    def getPricewithVAT(self):
        return (self.__price * 1.2)

#getter for private stockcode  
    def getStockcode(self):
        return self.__stockcode

#setter for public quantity        
    def setQuantity(self, quantity):
        self.quantity = quantity
    
#method to get quantity           
    def getQuantity(self):
        return self.quantity

#setter for public price           
    def setPrice(self, price):
        self.price = price
        
#method to get original price    
    def getPrice(self):
        return self.price   
         
#setter for public stockcode   
    def setStockcode(self, stockcode):
        self.stockcode = stockcode

#method to get stock code        
    def getStockcode(self):
        return self.stockcode

#setter for new price after quantity change         
    def setNewPrice(self, price):
        self.newprice = price
        
#method for users to enter new price, and then printing information with the new price        
    def getNewPrice(self):
         self.newprice = float(input("Enter new price per unit stock:\n"))
         return self.newprice
 
#setter for stock increment method             
    def setStockinc( self, Stockinc ):
        self.Stockinc = Stockinc

#getter for stock increment method          
    def getStockinc( self ):
        return self.Stockinc

#method to get VAT    
    def getVAT(self):
        return self.standardpercentageVAT
    
#method to get original price  with VAT    
    def getPricewithVAT(self):
        return (self.price * 1.2)
    
#method for getting new VAT price after users have inputted the new price
    def getNewVATPrice(self): 
        return (self.newprice * 1.2)

#method to get stock name    
    def getStockName(self):
        return self.stockname
    
#method to get stock description
    def getStockDescription(self):
        return self.stockdescription

#setter for new quantity of stock        
    def setNewQuantity(self, newquantity):
        self.newquantity = newquantity
        
#getter for new quantity of stock
    def getNewQuantity(self):
        if self.incordecdisplay1 == 1:
            return (self.Stockinc + self.quantity)
        elif self.incordecdisplay1 == 2:
            return (self.quantity - self.Stockdec)

#method to increase quantity of stock        
    def increaseStock(self):
#while loop to return the statement "Enter amount of stock(s) added:" if the right conditions for th code to proceed are not met
        isValidStockinc = False
        while not isValidStockinc:
            self.Stockinc = int(input("Enter amount of stock(s) added:\n"))
#an if statement to prompt users to enter a number that's not 0
            if 1> self.Stockinc:
                print("Error!, enter a number greater than or equal to 1")
#an elif statement to print an error message if the quantity is over 100 after adding stock
            elif (self.quantity + self.Stockinc) > 100:
                isValidStockinc = False
                print ("Error!, stock quantity above 100")
            else:
                isValidStockinc = True
                return (self.quantity + self.Stockinc)

#method to print new quantity of stock after users have added to it    
    def incDisplay(self):
        if self.increaseStock()> 100:
            print ("Error! stock level above 100")
            return self.incDisplay()
        else: 
            print (f"New stock quantity is {self.increaseStock()}")

#method to reduce amouunt of stock after stock have been sold            
    def sellStock(self):
#while loop to return the statement "Enter amount of stock(s) sold:" if the right conditions for th code to proceed are not met
        isValidStockdec = False
        while not isValidStockdec:
            self.Stockdec = int(input("Enter amount of stock(s) sold:\n"))
#if statement so users do not input a number below 1, or a number above the stock quantity. 
            if 1> self.Stockdec:
                print("Error!, enter a number greater than or equal to 1")
            elif self.Stockdec <= self.quantity:
                isValidStockdec = True
                return (self.quantity - self.Stockdec)
#also, an elif statement to print an error message if the subtraction leads to the stock quantity being below 1, 
#and then starting the while loop again
            elif (self.quantity - self.Stockdec) < 0:
                isValidStockdec = False
                print ("Error!, stock level below 1!")
            else:
                isValidStockdec = False
                print("Error!, stock level below 1")

#method to print new quantity of stock after users have reduced it            
    def decDisplay(self):
#if statement so it does not print 0 as a stock quantity
        if self.sellStock() != 0:
            print(f"New stock quantity is {self.sellStock()}")
        else:
            print("Error! stock level below 1")
            return self.decDisplay()

#method to ask users if they want to add or reduce stock amount    
    def incDecDisplay(self):
        self.incordecdisplay1 = int(input("Do you want to enter amount of stock added or sold. If added, enter 1, if sold, enter 2:\n"))
        if self.incordecdisplay1 == 1:
            return self.incDisplay()
        elif self.incordecdisplay1 == 2:
            return self.decDisplay()
        else:
            print("Error!, enter either 1 or 2")
            return self.incDecDisplay()
        
#method for choosing when to increase or reduce stock, based on the users input ( 1 to add, and 2 to reduce) 
#also created this method becaus my code was giving me errors in the subclass class section when I wanted it to increase or reduce stock... 
#...so changed it from a variable to a method
    def incorDecDisplay1(self):
        if self.incordecdisplay1() == 1:
            return (self.Stockinc + self.quantity)
        elif self.incordecdisplay1() == 2:
            return (self.quantity - self.Stockdec)
        else: 
            return self.incordecdisplay1()

#method to ask if users want to reduce stock after increasing stock earlier, and if not, they can end the code    
    def incDecDisplay2(self):
        self.incordecdisplay2 = int(input("If stock was sold as well, enter 1 to enter amount of stocks sold, else enter 2 to end\n"))
        if self.incordecdisplay2 == 1:
            return self.decDisplay2()
        elif self.incordecdisplay2 == 2:
            print ("")
        else:
            print("Error!, enter either 1 or 2")
            return self.incDecDisplay2()

#method to reduce stock after initially increasing it ( only if users want to do so)
#also created this method because my code was giving me errors in the subclass class section when I wanted it reduce stock after...
#...increasing it initially 
    def incorDecDisplay2(self):
        if self.incordecdisplay2() == 1:
            return (self.quantity - self.Stockdec)
            
        else: 
            return self.incordecdisplay2()

#method to ask if users want to add stock after reducing stock earlier , and if not, they can end the code          
    def incDecDisplay3(self):
        self.incordecdisplay3 = int(input("If stock was added as well, enter 1 to enter amount of stocks added, else enter 2 to end\n"))
        if self.incordecdisplay3 == 1:
            return self.incDisplay2()  
        elif self.incordecdisplay3 == 2:
            print ("")
        else:
            print("Error!, enter either 1 or 2")
            return self.incDecDisplay3()

#method to increase stock after initially reducing it ( only if users want to do so )   
#also created this method because my code was giving me errors in the subclass class section when I wanted it increase stock after...
#...reducing it initially   
    def incorDecDisplay3(self):
        if self.incordecdisplay3() == 1:
            return (self.Stockinc + self.quantity)
            
        else: 
            return self.incordecdisplay2()

#method to print stock information after decreasing it    
    def decDisplay2(self):
        if self.sellStock() > 0:
            print("Stock category:"+self.StockCategory+ f"\nNew stock quantity: {self.sellStock()} \nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT:  {self.getNewVATPrice()}")
        else:
            print("Error!, stock level below 1")
            return self.decDisplay2()
            
            
            
#method to print stock information after increasing it
    def incDisplay2(self):
            print("Stock category:" +self.StockCategory+ f"\nNew stock quantity: {self.increaseStock()} \nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT:  {self.getNewVATPrice()}")

#method to print code after the initial price has been chnaged to a new one (after change in quantity)    
    def newPrice(self):
            print("\nStock category:" +self.StockCategory+ f"\nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nTotal units in stock: {self.getNewQuantity()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT: {self.getNewVATPrice()}")

#method to check if stock was added or reduced earlier, then asks users if they want to do the opposite 
#(i.e  if it was added earlier return to line 126, else return to line 137 )   
    def incDec2or3(self):
        if self.incordecdisplay1 == 1:
            return self.incDecDisplay2() 
            
        elif self.incordecdisplay1 == 2:
            return self.incDecDisplay3()
        
#method to print the information of the stock
    def __str__(self):
        return "Stock category:" +self.StockCategory + f"\nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nTotal units in stock: {self.getQuantity()} \nPrice without VAT: {self.getPrice()} \nPrice with VAT:  {self.getPricewithVAT()}"

#SUBCLASS SECTION
          
class NavSys (StockItem):
    
    def __init__(self, brand,quantity, price, stockcode, stockname, stockdescription ):
#calling the super class's constructor, and stating the attributes we want from it
        super().__init__(quantity, price, stockcode, stockname , stockdescription, standardpercentageVAT = 1.2)
        self.__brand = brand
        self.stockname = stockname
        self.stockdescription = stockdescription 

#overriding the superclass's "getStockName" method to print something different
    def getStockName(self):
        return "Navigation system"
#overriding the superclass's "getStockDescription" method to print something different    
    def getStockDescription(self):
        return "GeoVision Sat Nav"
        
#setting the brand name    
    def setnavsysBrand (self, brand):
        self.__brand = brand

#getting the brand name     
    def getnavSysBrand (self):
        return self.__brand

#overriding the superclass's "incDisplay2" method to print stock informartion with brand name included  
    def incDisplay2(self):
        print("Stock category:" +self.StockCategory+ f"\nNew stock quantity: {self.increaseStock()} \nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT:  {self.getNewVATPrice()} \nBrand: {self.getnavSysBrand()}")

#overriding the superclass's "decDisplay2" method to print stock information with brand name included     
    def decDisplay2(self):
        if self.sellStock() > 0:
            print("Stock category:"+self.StockCategory+ f"\nNew stock quantity: {self.sellStock()} \nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT:  {self.getNewVATPrice()} \nBrand: {self.getnavSysBrand()}")
        else:
            print("Error!, stock level below 1")
            return self.decDisplay2()
    
    def newPrice(self):
            print("\nStock category:" +self.StockCategory+ f"\nStock type: {self.getStockName()} \nStock code: {self.getStockcode()}  \nDescription: {self.getStockDescription()} \nTotal units in stock: {self.getNewQuantity()} \nPrice without VAT: {self.getNewPrice()} \nPrice with VAT: {self.getNewVATPrice()} \nBrand: {self.getnavSysBrand()}")

#calling the super class's method, which it uses to print stock information, and  overriding it by adding a new attribute, "brand name"   
    def __str__(self):
        return super().__str__() + "\nBrand:" +str (self.getnavSysBrand())
 

stockname = str(input("Enter stock name:\n"))

stockcode = str(input("Enter stock code:\n"))

stockdescription =str(input("Enter stock description :\n"))

#code to restrict the quantity of stock users input to stay between 1 and 100 ( 1 and 100 included)    
isValidQuantity = False
while not isValidQuantity:
 quantity = int(input("Enter stock quantity here:\n"))
 if quantity <1:
     print("Error!, quantity of stock cannot be below 1")
 elif quantity >100:
     print("Error!, quantity of stock cannot be above 100")
 else:
     isValidQuantity = True

price = float(input("Enter stock price per unit:\n"))


#instance of the super class StockItem
item1 = StockItem(quantity , price , stockcode, stockname,stockdescription, standardpercentageVAT= 1.2)
print(item1)
print("\n")
item1.incDecDisplay()                
item1.newPrice()
print("\n")
item1.incDec2or3()
print ("\nNAVSYS(SUBCLASS) SECTION:")

brand = str(input("Enter NavSys brand name here:\n"))

#code to restrict the quantity of stock users input to stay between 1 and 100 ( 1 and 100 included)
isValidQuantity = False
while not isValidQuantity:
 quantity = int(input("Enter stock quantity here:\n"))
 if quantity <1:
     print("Error!, quantity of stock cannot be below 1")
 elif quantity >100:
     print("Error!, quantity of stock cannot be above 100")
 else:
     isValidQuantity = True
price = float(input("Enter stock price per unit:\n"))

#instance of the sub class NavSys
item2 = NavSys(brand, quantity, price, stockcode="NS101", stockname="Navigation system", stockdescription="GeoVision Sat Nav")
print(item2)
print("\n")
item2.incDecDisplay()
item2.newPrice()
print("\n")
item2.incDec2or3()
    
#NOTE: 
# I made the code user interactive instead of having fixed values inputted by me because I thought that would be better, and more realistic.
# also, when users have chosen whether they want to add or reduce stock, for reasons unknown to me...
#...the code asks for the amount they want to add or reduce twice, so whoever is running this code should enter the amount two different times